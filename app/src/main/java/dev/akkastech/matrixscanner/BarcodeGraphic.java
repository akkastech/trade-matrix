/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.akkastech.matrixscanner;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.Log;

import dev.akkastech.matrixscanner.camera.GraphicOverlay;

import com.google.android.gms.vision.barcode.Barcode;

import static android.content.ContentValues.TAG;

/**
 * Graphic instance for rendering barcode position, size, and ID within an associated graphic
 * overlay view.
 */
public class BarcodeGraphic extends GraphicOverlay.Graphic {

    private int mId;
    private Context context;
    Paint mTxtPaint = new Paint();



    private static final int COLOR_CHOICES[] = {
            Color.BLUE,
            Color.CYAN,
            Color.GREEN
    };

    private static int mCurrentColorIndex = 0;

    private Paint mRectPaint;
    private Paint mTextPaint;
    private volatile Barcode mBarcode;

    final String[] data = {""};


    BarcodeGraphic(GraphicOverlay overlay) {
        super(overlay);

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mRectPaint = new Paint();
        mRectPaint.setColor(selectedColor);
        mRectPaint.setStyle(Paint.Style.STROKE);
        mRectPaint.setStrokeWidth(4.0f);

        mTextPaint = new Paint();
        mTextPaint.setColor(selectedColor);
        mTextPaint.setTextSize(36.0f);
        //context = this;

    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public Barcode getBarcode() {
        return mBarcode;
    }

    /**
     * Updates the barcode instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateItem(Barcode barcode) {
        mBarcode = barcode;
        final String[] rst = {""};
        String[] separated = barcode.rawValue.split("/");
        if((separated[0].compareTo("https:") == 0 || separated[0].compareTo("http:") == 0) && (separated[2].compareTo("database.matrixtooling.com") == 0)){
            separated[4] = separated[4].substring(0,separated[4].length() -1);
            String title = separated[4];
            separated[4] = separated[4] + "_info";
            final String[] result = {TextUtils.join("/", separated)};
            Log.d(TAG, "updateItem: "+ result[0]);
            Log.d(TAG, "updateItem: "+ this.context);
            title = toTitleCase(title);
            data[0] = title+": "+separated[5];

        }else{
            data[0] = "Invalid Barcode";
        }

        //Log.d(TAG, "updateItem: "+data[0]);

        postInvalidate();
    }

    public static String toTitleCase(String s)
    {
        if (s.isEmpty())
        {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    /**
     * Draws the barcode annotations for position, size, and raw value on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Barcode barcode = mBarcode;
        String result = data[0];
        if (barcode == null) {
            return;
        }

        // Draws the bounding box around the barcode.
        RectF rect = new RectF(barcode.getBoundingBox());
        rect.left = translateX(rect.left);
        rect.top = translateY(rect.top);
        rect.right = translateX(rect.right);
        rect.bottom = translateY(rect.bottom);
        canvas.drawRect(rect, mRectPaint);

        // Draws a label at the bottom of the barcode indicate the barcode value that was detected.
        //Log.d(TAG, "draw: " + barcode.rawValue);

        //canvas.drawRect(result, rect.left,rect.bottom,rect.bottom,  mTextPaint);
        //canvas.drawText(result, rect.left, rect.bottom, mTextPaint);

        Paint.FontMetrics fm = new Paint.FontMetrics();
        mTxtPaint.setColor(Color.WHITE);
        mTxtPaint.setTextSize(38.0f);
        mTxtPaint.getFontMetrics(fm);

        int margin = 25;

        canvas.drawRect(rect.left, rect.bottom, rect.left + 300 , rect.bottom - fm.top + 40, mTxtPaint);


        mTxtPaint.setColor(Color.BLACK);
        //mTextPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        canvas.drawText(result, rect.left + margin, rect.bottom + 50, mTxtPaint);
//        if(result != "Invalid Barcode" && result != ""){
//            canvas.drawText("Tap to fetch", rect.left + margin, rect.bottom + 100, mTxtPaint);
//            canvas.drawText("information", rect.left + margin, rect.bottom + 150, mTxtPaint);
//        }


    }
}
