package dev.akkastech.matrixscanner;


public interface BarCodeDetectListener {
    void onBarCodeDetect(String barcode);
}
